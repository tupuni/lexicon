# Contributing to the Lexicon
The Lexicon data is stored in multiple locations:
 - a CSV table [lexicon.csv](https://gitlab.huma-num.fr/tupuni/lexicon/-/blob/main/lexicon.csv)
 - photographs or graphic representations of each entry in the [figures folder](https://gitlab.huma-num.fr/tupuni/lexicon/-/tree/main/figures)
 - a bibliographical reference list in the [dedicated .bib file](https://gitlab.huma-num.fr/tupuni/lexicon/-/blob/main/lexicon_refs.bib)

Adding or updating data in the Lexicon requires following specific guidelines for each of these locations. 

## 1 The Lexicon table
The table is made of 9 columns: 
- the first four columns are dedicated to the lexical items in English (`EN`), French (`FR`), German (`DE`), and Spanish (`ES`).
- `Description` for a unified semantic value (in english).
- `Figures` listing unique identifiers pointing at image in the [figures folder](https://gitlab.huma-num.fr/tupuni/lexicon/-/tree/main/figures). 
- `References` listing unique identifiers pointing at reference code in the [.bib file](https://gitlab.huma-num.fr/tupuni/lexicon/-/blob/main/lexicon_refs.bib).
- `See-also` pointing at other related entries in the Lexicon.
- `Note` for free comments.

In the CSV file, columns are separated by commas (`,`), and items within the same column (such as Figure codes and Reference codes) are separated by a semicolons (`;`). The use of commas within a text must be handled as a special case, with the text wrapped within double quotation marks (`"..."`)

Therefore, adding a new row in the table must be done using the **Edit** button and using the following pattern: 9 columns, 8 separators, and quotation marks in the 5th `Description` column:
```
EN,FR,DE,ES,Description,Figures,References,See-also,Note
,,,,"",,,,
```
| EN | FR | DE | ES | Description | Figures | References | See-also | Note |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|     |     |     |     |     |     |     |     |     |

### 1-1 Procedure to add a new row
1) Open the [lexicon.csv file](https://gitlab.huma-num.fr/tupuni/lexicon/-/blob/main/lexicon.csv) and click the **Edit** button.  
![edit-table](archives/edit-table.png) 

2) Press `Enter` on your keyboard to add a new entry (please follow alphabetical order).  

3) Then add the following line : `,,,,"",,,,`
![edit-row](archives/edit-row.png) 

4) Start editing this new row following the pattern described above.

5) Please save ("commit") every change after adding/modifying each lexical entry.  
If you would like to modify more than one row in the table entry, please do so for every single entry/row: indicate your action in a short descriptive message in the **Commit message** (example: `Add item x`, or `Add DE version for item x`, or `Modify description y`) before pressing **Commit changes** (on the main branch).

### 1-2 Procedure to suggest modifications
1) Open an Issue : In the left pannel click on **Issues**, 
![issue1](archives/issue1.png) 

2) Click on the **New Issue** button at the top right.
![issue2](archives/issue2.png) 

3) Use the `Lexical item` as the **Title** for your issue.  

4) In the issue **Description**, enter the path to the Lexicon entry in the table in the issue description using the following pattern:  
https://gitlab.huma-num.fr/tupuni/lexicon/-/blob/main/lexicon.csv + **line number**  (example **#L1** for the first row):
```
https://gitlab.huma-num.fr/tupuni/lexicon/-/blob/main/lexicon.csv#L1
```
5) Describe your issue in a short **Commit message**.  

6) If necessary, assign one or several collaborator-s to that issue in order to explicitaly encourage them to participate in the discussion below (see the **Assignee** button).

7) Click on **Create issue**.

8) Start commenting.

## 2 The Lexicon figures
Each Lexicon entry should be illustrated with an associated figure in the [dedicated folder](https://gitlab.huma-num.fr/tupuni/lexicon/-/tree/main/figures).  

1) Name your picture according to the unique identifier(s) in the related Lexicon entry - see the `Figures` column of the **Lexicon table**.  
*NB 1 : The figure file name and unique identifier should be created according to the following pattern : entryname_referencecode.extension  
Example : splitfracture_faivre2010_1.png*.  
*NB 2 : Make sure the picture quality is sufficient (min 300 dpi).*  
*NB 3 : Accepted extensions are jpg, png, tiff ou pdf. Please don't use proprietary formats such as **.ai** or **.ps**.*

2) Add your picture in the [**Figures** folder](https://gitlab.huma-num.fr/tupuni/lexicon/-/tree/main/figures) using the **Upload** function.
![upload](archives/upload.png) 

3) Insert the unique identifier in the related Lexicon entry in the [`lexicon.csv` file](https://gitlab.huma-num.fr/tupuni/lexicon/-/blob/main/lexicon.csv).

## 3 The Lexicon references
1) Open the [Zotero Group Library](https://www.zotero.org/groups/5548572/lexicon), using either the [Web Application](https://www.zotero.org) or the [Desktop Application](https://www.zotero.org/support/installation).  

2) Add the new reference details and the Reference unique identifier **as a note**.  
*NB : Reference code = first author name + year of publication (ex: faivre2010)*.  
![zotero](archives/zotero.png) 

3) Insert the unique identifier in the related Lexicon entry in the [`lexicon.csv` file](https://gitlab.huma-num.fr/tupuni/lexicon/-/blob/main/lexicon.csv).

4) **Do not modify the .bib file in the repository**. The Lexicon maintainers will update the .bib file based on newly added references in the Zotero Group Library.  
